globals [
  notas
  iterador
  totalGraduados
  mAprobadas
  numEnPrueba
  numNoPrueba
  desertores
  desertoresXSemestre
  desertoresXTrabajo
  noProblemas
  probalidad
  desercionXPruebas
  desercionXPruebasXSemestre
]

turtles-own [
  promedio
  dificultadProfesores
  gustoPensum
  cargaAcademica
  materiasAprobadas
  materiasPerdidas
  nivelColegio
  tiempoEstudio
  trabaja
  tiempoDeOcio
  estratoSocial
  estadoSalud
  tiempoDesplazamiento
  motivacion
  totalMatAprob

  iteradorDesertar
  estaEnPrueba
  contadorPruebas
  pruebasConsecutivas
]


to inicializar
  ca
  crt numeroEstudiantes [
    set shape "person" set size 1 set xcor 0 set ycor (random -55) - 3 set color gray set heading 90 set estaEnPrueba false set contadorPruebas 0 set pruebasConsecutivas 0 set totalMatAprob 0

    ; asignacion de nivel del gusto sobre el pensum
    set probalidad obtenerProbabilidad
    if (probalidad <= 5 )[set gustoPensum 0]
    if (probalidad <= 30 and probalidad > 5 )[set gustoPensum 1]
    if (probalidad <= 100 and probalidad > 30)[set gustoPensum 2]

    ; asignacion de nivel del colegio anterior
    set probalidad obtenerProbabilidad
    if (probalidad <= 20)[set nivelColegio 0]
    if (probalidad > 20 and probalidad <= 50)[set nivelColegio 1]
    if (probalidad > 50 and probalidad <= 100)[set nivelColegio 2]

    ; asignacion de tiempo de desplazamiento hasta la universidad
    set probalidad obtenerProbabilidad
    if (probalidad <= 25)[set tiempoDesplazamiento 0] ; 20% mucho
    if (probalidad > 25 and probalidad <= 85)[set tiempoDesplazamiento 1] ; 60% normal
    if (probalidad > 85 and probalidad <= 100)[set tiempoDesplazamiento 2] ; 15% poco

    ; asignacion de estrato social
    set probalidad obtenerProbabilidad
    if (probalidad <= 2)[set estratoSocial 0] ; 2%
    if (probalidad > 2 and probalidad <= 7)[set estratoSocial 1] ; 5%
    if (probalidad > 7 and probalidad <= 19)[set estratoSocial 2] ; 12%
    if (probalidad > 19 and probalidad <= 47)[set estratoSocial 3] ; 28%
    if (probalidad > 47 and probalidad <= 82)[set estratoSocial 4] ; 35%
    if (probalidad > 82 and probalidad <= 94)[set estratoSocial 5] ; 12%
    if (probalidad > 94 and probalidad <= 99)[set estratoSocial 6] ; 5%
    if (probalidad > 99 and probalidad <= 100)[set estratoSocial 7] ; 1%
  ]

  ; inicializar variables
  set iterador 0
  set NumNoPrueba 0
  set NumEnPrueba 0
  set desertores 0
  set desertoresXSemestre 0
  set noProblemas 0
  set desercionXPruebas 0
  set desercionXPruebasXSemestre 0
  set desertoresXTrabajo 0
  makeWall
  reset-ticks
end


to go
  if (iterador < 135) [

    ask turtles [set iteradorDesertar 0]
    asigVarXStep
    validarCondStep
    if (iterador < 18)[validarCondPrRen]
    ;-------------------------- renovacion 1 ------------------------------
    if (iterador = 8) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 2 ------------------------------
    if (iterador = 17) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 3 ------------------------------
    if (iterador = 27) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 4 ------------------------------
    if (iterador = 36) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 5 ------------------------------
    if (iterador = 45) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 6 ------------------------------
    if (iterador = 54) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 7 ------------------------------
    if (iterador = 63) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 8 ------------------------------
    if (iterador = 72) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 9 ------------------------------
    if (iterador = 81) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 10 ------------------------------
    if (iterador = 90) [asigVarXSem  finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 11 ------------------------------
    if (iterador = 99) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 12 ------------------------------
    if (iterador = 108) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 13 ------------------------------
    if (iterador = 117) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 14 ------------------------------
    if (iterador = 126) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]
    ;-------------------------- renovacion 15 ------------------------------
    if (iterador = 135) [asigVarXSem finalizarRenovacion set desertoresXSemestre 0]

    ask turtles with [color = gray][fd 1]
    set iterador (iterador + 1)

  ]
  if (iterador = 135) [
    ask turtles with [color = 64][set totalGraduados totalGraduados + 1]
    ask turtles with [color = gray][set color red set desertores desertores + 1 set desertoresXSemestre desertoresXSemestre + 1]
    set iterador (iterador + 1)
  ]
end

to validarCondStep
  ; si esta enfermo
  ask turtles with [color = gray and estadoSalud = 0][set color red set desertores (desertores + 1) set desertoresXSemestre (desertoresXSemestre + 1)]
end

to validarCondPrRen
  ; si no le gusta la carrera y esta desmotivado
  ask turtles with [color = gray and gustoPensum = 0 and motivacion = 0][
    set probalidad obtenerProbabilidad if(probalidad < 20) [set color red set desertores (desertores + 1) set desertoresXSemestre (desertoresXSemestre + 1)] ; 20% deserta
  ]

  ; si el nivel del colegio es malo y la dificultad de los profesores es dificil
  ask turtles with [color = gray and nivelColegio = 0 and dificultadProfesores = 2][
    set probalidad obtenerProbabilidad if(probalidad < 100) [set color red set desertores (desertores + 1) set desertoresXSemestre (desertoresXSemestre + 1)]
  ]

  ; si la universidad le queda lejos de la casa y tiene recursos para el transporte y la matricula
  ask turtles with [color = gray and estratoSocial < 3 and tiempoDesplazamiento = 0][
    set probalidad obtenerProbabilidad if(probalidad < 30) [set color red set desertores (desertores + 1) set desertoresXSemestre (desertoresXSemestre + 1)]
  ]
end

to validarCondUltRen
  ; si estrato economico es menor que 3 y tiene trabajo
  ask turtles with [color = gray and estratoSocial < 3 and trabaja = true][
    set probalidad obtenerProbabilidad if(probalidad < 20) [set color red set desertores (desertores + 1) set desertoresXSemestre (desertoresXSemestre + 1) set desertoresXTrabajo (desertoresXTrabajo + 1)] ; 20% deserta
  ]

end

to finalizarRenovacion
  validarCondUltRen
  set desercionXPruebasXSemestre 0
  set numEnPrueba 0
  set numNoPrueba 0
  ask turtles with [color = gray][
    ifelse(materiasPerdidas >= pruebasConsecPermitidas or promedio < 32)[
      if(estaEnPrueba) [set pruebasConsecutivas pruebasConsecutivas + 1]
      set contadorPruebas contadorPruebas + 1
      set estaEnPrueba true
      set numEnPrueba numEnPrueba + 1
    ][
      set estaEnPrueba false
      set pruebasConsecutivas 0
      set numNoPrueba numNoPrueba + 1
    ]
    if(totalMatAprob > numMateriasTotales)[set color 64]
    if(pruebasConsecutivas >= 3)[set color red set desercionXPruebas desercionXPruebas + 1 set desertores desertores + 1 set desercionXPruebasXSemestre desercionXPruebasXSemestre + 1 set desertoresXSemestre desertoresXSemestre + 1]
  ]
 tick
end


; se ejecuta cuando pasa 1 step (1 renovacion tiene 9 steps) para asignar un nuevo valor a estas caracteristicas
to asigVarXStep
  ask turtles with [color = gray][
    ; asignacion de tiempo de ocio
    let proTO obtenerProbabilidad
    if (proTO <= 5)[set tiempoDeOcio 0] ; 5% poco
    if (proTO <= 45 and proTO > 5)[set tiempoDeOcio 1] ; 40% normal
    if (proTO <= 80 and proTO > 45)[set tiempoDeOcio 2] ; 35% mucho
    if (proTO <= 100 and proTO > 80)[set tiempoDeOcio 3] ; 20% extremo

    ; asignacion del estado de salud
    let proES obtenerProbabilidad
    if (proES = 0)[
      set proES obtenerProbabilidad
      if (proES  = 0) [set estadoSalud 0]] ; 0,1% enfermo
    if (proES <= 100 and proES > 0)[set estadoSalud 1] ; 99,9% sano

    ; asignacion del nivel de motivacion
    let proM obtenerProbabilidad
    if (proM <= 25)[set motivacion 0] ; 25% muy motivado
    if (proM <= 85 and proM > 25)[set motivacion 1] ; 60% motivado
    if (proM <= 100 and proM > 85)[set motivacion 1] ; 15% desmotivado

    ; asignacion de nivel del gusto sobre el pensum
    let proGP obtenerProbabilidad
    if (proGP <= 5 )[set gustoPensum 0]
    if (proGP <= 30 and proGP > 5 )[set gustoPensum 1]
    if (proGP <= 100 and proGP > 30)[set gustoPensum 2]
  ]
end

; se ejecuta cuando pasa 1 semestre para asignar un nuevo valor a estas caracteristicas
to asigVarXSem
  ask turtles with [color = gray][
    ; asignacion de tiempo de estudio
    let proTE obtenerProbabilidad
    if (proTE <= 10)[set tiempoEstudio 2]
    if (proTE <= 50 and proTE > 10)[set tiempoEstudio 1]
    if (proTE <= 100 and proTE > 50)[set tiempoEstudio 0]

    ; asignacion de tiempo de desplazamiento hasta la universidad
    let proTD obtenerProbabilidad
    if (proTD <= 25)[set tiempoDesplazamiento 0] ; 20% mucho
    if (proTD > 25 and proTD <= 85)[set tiempoDesplazamiento 1] ; 60% normal
    if (proTD > 85 and proTD <= 100)[set tiempoDesplazamiento 2] ; 15% poco

    ; asignacion de dificultad de profesores
    let proDP obtenerProbabilidad
    if (proDP <= 25)[set dificultadProfesores 0] ; 25% facil
    if (proDP > 25 and proDP <= 75)[set dificultadProfesores 1] ; 50% normal
    if (proDP > 75 and proDP <= 100)[set dificultadProfesores 2] ; 25% dificil

    ; asignacion de numero de materias aprobadas
    let proNM obtenerProbabilidad
    if (proNM <= 5)[set materiasAprobadas 3] ; 5%
    if (proNM > 5 and proNM <= 20)[set materiasAprobadas 4] ; 15%
    if (proNM > 20 and proNM <= 40)[set materiasAprobadas 5] ; 20%
    if (proNM > 40 and proNM <= 65)[set materiasAprobadas 6] ; 25%
    if (proNM > 65 and proNM <= 95)[set materiasAprobadas 7] ; 30%
    if (proNM > 95 and proNM <= 100)[set materiasAprobadas 8] ; 5%

    ; asignacion de numero de materias reprobadas
    set materiasPerdidas (8 - materiasAprobadas)
    set totalMatAprob totalMatAprob + materiasAprobadas

    ; asignacion de promedio
    let proP obtenerProbabilidad
    if (proP <= 35)[set promedio (random 10) + 40] ; 35%  (notas entre 40 - 50)
    if (proP > 35 and proP <= 80)[set promedio (random 5) + 35] ; 45% (notas entre 35 - 40)
    if (proP > 80 and proP <= 100)[set promedio random 35] ; 20% (notas entre 0 - 35)

    ; asignacion de si trabaja o no
    let proTB obtenerProbabilidad
    ifelse(proTB < 10)[set trabaja true][set trabaja false]; 10 %
  ]
end

to-report obtenerProbabilidad
  report random 100
end

to asignarVariablesAcademicasPrimerSemestre
  ask turtles with [color = gray][
    set promedio (random 30) + 20
    ;set calidadProfesores random 5
    set cargaAcademica (random 5) + 3
    ;materias cargaAcademica
  ]
end

; -------------------- mapa de la simulacion ------------------------------------------------------------
to makeLineaRenovacion [xCord]
  ask patches[
    if  (pxcor = xCord) and (pycor >= -57 and pycor <= -3) [set pcolor white]
  ]
end

to makeWall

  ; Muros
  ask patches[
    if  (pxcor >= 0 and pxcor <= 135) and (pycor >= -2 and pycor <= 0) [set pcolor 85]
  ]

  ask patches[
    if  (pxcor >= 0 and pxcor <= 135 ) and (pycor >= -60 and pycor <= -58) [set pcolor 85 ]
  ]

  ask patches[
    if  (pxcor = 135) and (pycor >= -57 and pycor <= -3) [set pcolor 85 ]
  ]

  ; Renovaciones
  let i 0
  loop [
    if i = 15 [ stop ]
    makeLineaRenovacion 9 * i
    set i i + 1
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
263
10
2039
812
-1
-1
13.0
1
10
1
1
1
0
1
1
1
0
135
-60
0
0
0
1
ticks
30.0

BUTTON
164
296
229
329
go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
0
217
259
277
numeroEstudiantes
140.0
1
0
Number

BUTTON
6
296
69
329
setup
inicializar
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
0
827
958
1120
Datos por semestre
renovacion
num estudiantes
0.0
15.0
0.0
10.0
true
true
"" ""
PENS
"Prueba" 1.0 1 -1264960 true "" "plot numEnPrueba"
"No Prueba" 1.0 0 -6759204 true "" "plot numNoPrueba"
"Pierde Calidad Estudiante" 1.0 0 -2674135 true "" "plot desertoresXSemestre"
"Pierde Calidad x Pruebas" 1.0 0 -12186836 true "" "plot desercionXPruebasXSemestre"

PLOT
989
829
1769
1123
Datos acumulados
renovacion
estudiantes
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"Graduados" 1.0 1 -14439633 true "" "plot count turtles with [color = 64]"
"Pierde calidad estudiante" 1.0 0 -5298144 true "" "plot desertores"
"Pierde calidad x pruebas" 1.0 0 -2139308 true "" "plot desercionXPruebas"
"Pierde calidad x trabajo" 1.0 0 -10873583 true "" "plot desertoresXTrabajo"

SLIDER
8
354
258
387
NumMateriasTotales
NumMateriasTotales
40
100
74.0
1
1
NIL
HORIZONTAL

SLIDER
8
404
257
437
PruebasConsecPermitidas
PruebasConsecPermitidas
2
6
3.0
1
1
NIL
HORIZONTAL

SLIDER
6
450
255
483
PromedioMinimo
PromedioMinimo
25
50
32.0
1
1
NIL
HORIZONTAL

BUTTON
83
296
148
330
step
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

@#$#@#$#@
## Funcionamiento
Para iniciar la simulacion es necesario primero hacer click en el boton 'setup' que inicializara variables, despues click al boton 'go', de esta manera empezara la simulacion con los valores por defecto. No obstante es posible cambiar parametros que alteran de manera significativa la simulacion, tales como: la cantidad de  pruebas consecutivas permitidas antes de perder la calidad de estudiante, el promedio minimo para no entrar en prueba, el numero de materias totales para poder graduarse y la cantidad de estudiantes en la simulacion.

## Descripcion

Esta es la simulacion de la permanencia/desercion academica de los estudiantes en la facultad de ingenieria. La simulacion arranca ubicando los estudiantes en la parte izquierda de la pantalla de color gris, a medida que avanza el semestre los estudiantes avanzan hacia la derecha. En la animacion se puede observar 15 lineas blancas que representan las renovaciones que tienen disponibles los estudiantes. Un estudiante inicialmente tiene el color gris, cuando este deserta cambia a color rojo y deja de avanzar, cuando un estudiante se gradua (significa que alcanzo el aprobo el numero de materias totales) toma el color verde. Un estudiuante puede desertar por diferentes maneras, en los diferentes semestres se tienen en cuenta diferentes variables, siendo la variable que mas provoca desercion, la acumulacion de pruebas academicas consecutivas.

Al precionar el boton de set up se inicializa algunas variables aleatoriamente, basandonos en probabilidades establecidias en el siguiente apartado. Y tambien se re asignan algunas variables en cada step y en cada renovacion (1 renovacion tiene 9 steps) de manera que podemos hacer validaciones en cada step y en cada renovacion. 

## Variables academicas
- Promedio (0 - 50)
-- 40 - 50 -> 35%
-- 35 - 40 -> 45%
-- 0  - 35 -> 20%
- Dificultad profesores (0 - 3)
-- 0 Facil ->  25%
-- 1 Normal -> 50%
-- 2 Dificil -> 25%
- Gusto de pensum (0 - 3)
-- 0 No le gusta la carrera
-- 1 Le da igual el pensum
-- 2 Le gusta la carrera
- Numero de materias aprobadas (3 - 8)
-- 3  5%
-- 4  15%
-- 5  20%
-- 6  25%
-- 7  30%
-- 8  5%
- Numero de materias perdidas (3 - 8)
-- se calcula restando la cant max de materias por semestre (8) menos la cantidad de materias aprobadas
- Nivel del colegio (0 - 3)
-- 0 Malo 35%
-- 1 Normal 50%
-- 2 Bueno 15%
- Tiempo de estudio (0 - 2)
-- 0 No estudia
-- 1 Estudia normal
-- 2 Estudia mucho
## Variables personales
- Trabaja (si - no)
-- si 10%
-- no 90%
- Tiempo de ocio (0 - 4)
-- Poco 5%
-- Normal 40%
-- Mucho 35%
-- Extremo 20%
- Estrato economico (0 - 7)
-- 0	2%
-- 1	5%
-- 2	12%
-- 3	28%
-- 4	35%
-- 5	12%
-- 6	5%
-- 7	1%
- Estado de salud (0 - 1)
-- 0 Enfermo 0,1%
-- 1 Sano 99,9%
- Tiempo de desplazamiento (0 - 2)
-- 0 Mucho 25%
-- 1 Normal 60%
-- 2 Poco 15%	
- Motivacion (0 - 2)
-- Muy motivado 25%
-- Motivado 60%
-- Desmotivado 15%

## Creadores
- Josue Nunez Prada 20172020071
- Andres Felipe Wilches
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
