import pandas as pd
from sklearn import metrics

df = pd.read_excel('Dataset_Prueba.xlsx')
df.head()



## Attributes

# STG
# SCG
# STR
# LPR
# PEG

# rules
classified_class = []

for i in range(0,df.shape[0]):
    # root
    if df['STG'].iloc[i] > 0.327:
        # right node #3
        if df['SCG'].iloc[i] > 0.42:
            # right node #7
            if df['STR'].iloc[i] > 0.5: 
                # right node #15
                if df['LPR'].iloc[i] > 0.365:
                    # right node #31
                    if df['PEG'].iloc[i] > 0.54:
                        # right leaf
                        classified_class.append('High')
                    else:
                        #right node #60
                        if df['STG'].iloc[i] > 0.495:
                            if df['SCG'].iloc[i] > 0.46:
                                if df['STR'].iloc[i] > 0.68:
                                    # right leaf
                                    classified_class.append('Middle')
                                else:
                                    # left leaf
                                    classified_class.append('Low')
                            else:
                                # left leaf
                                classified_class.append('Low')
                        #right node #60
                        else:
                            # white last table 4 att 0.495
                            if df['SCG'].iloc[i] > 0.495:
                                if df['STR'].iloc[i] > 0.555:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('High')
                            else:
                                classified_class.append('Low')
                else:
                    # node 30, orange
                    if df['PEG'].iloc[i] > 0.61:
                        # node 59 orange
                        if df['STG'].iloc[i] > 0.5175:
                            # right 0.505
                            if df['SCG'].iloc[i] > 0.505:
                                # 0.68
                                if df['STR'].iloc[i] > 0.68:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                            else:
                                classified_class.append('Middle')
                        else:
                            # left 0.7 
                            if df['SCG'].iloc[i] > 0.7:
                                # 0.68
                                if df['STR'].iloc[i] > 0.745:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                            else:
                                if df['STR'].iloc[i] > 0.805:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                    else:
                        # upper part of last orange table
                        # node 58
                        if df['STG'].iloc[i] > 0.48:
                            # 0.52
                            if df['SCG'].iloc[i] > 0.52:
                                classified_class.append('Middle')
                            else:
                                # 0.665
                                if df['STR'].iloc[i] > 0.665:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                        else:
                            # 0.385
                            if df['SCG'].iloc[i] > 0.385:
                                # 0.7
                                if df['STR'].iloc[i] > 0.7:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                            else:
                                # 0.625
                                if df['STR'].iloc[i] > 0.625:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
            # left part umbral 7
            else:
                # right part umbral #14
                if df['LPR'].iloc[i] > 0.345:
                    # right part umbral #29
                    # yellow
                    if df['PEG'].iloc[i] > 0.5:
                        # right leaf
                        classified_class.append('High')
                    else:
                        # node 57 0.435
                        if df['STG'].iloc[i] > 0.435:
                            # 0.63
                            if df['SCG'].iloc[i] > 0.63:
                                # 0.395
                                if df['STR'].iloc[i] > 0.395:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                            else:
                                # 0.392
                                if df['STR'].iloc[i] > 0.395:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                        else:
                            # 0.485
                            if df['SCG'].iloc[i] > 0.485:
                                classified_class.append('Low')
                            else:
                                # 0.21
                                if df['STR'].iloc[i] > 0.21:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('very_low')
                else:
                    # left part umbral 28 right white 0.62
                    if df['PEG'].iloc[i] > 0.62:
                        # node 56 0.4525
                        if df['STG'].iloc[i] > 0.4525:
                            # white 0.61
                            if df['SCG'].iloc[i] > 0.61:
                                # 0.385 white low table
                                if df['STR'].iloc[i] > 0.385:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('High')
                            else:
                                classified_class.append('Middle')
                        else:
                            classified_class.append('High')
                    # left white 0.62
                    else:
                        # node 55 0.55
                        if df['STG'].iloc[i] > 0.55:
                            # white 0.455
                            if df['SCG'].iloc[i] > 0.455:
                                # 0.22 white low table
                                if df['STR'].iloc[i] > 0.22:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                            else:
                                classified_class.append('Low')
                        else:
                            # white 0.595
                            if df['SCG'].iloc[i] > 0.595:
                                # 0.245 white low table
                                if df['STR'].iloc[i] > 0.245:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                            else:
                                # 0.395 white low table
                                if df['STR'].iloc[i] > 0.395:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
        ##right right part
        # now, left part node 3 
        else:
            # node 6 0.445
            if df['STR'].iloc[i] > 0.445:
                # node 13 0.32
                if df['LPR'].iloc[i] > 0.32: 
                    # node 27 0.35
                    if df['PEG'].iloc[i] > 0.35:
                        # node 54 0.4775
                        if df['STG'].iloc[i] > 0.4775:  
                            classified_class.append('High')
                        else:
                            # blue white node 0.2545
                            if df['SCG'].iloc[i] > 0.2545:
                                classified_class.append('Middle')
                            else:
                                # blue white node 0.815
                                if df['STR'].iloc[i] > 0.815:
                                    classified_class.append('Middle')
                                else:                               
                                    classified_class.append('High')
                    # left part of node 27
                    else:
                        # node 53 0.565
                        if df['STG'].iloc[i] > 0.565:
                            # node 0.199
                            if df['SCG'].iloc[i] > 0.199:
                                # node 0.555
                                if df['STR'].iloc[i] > 0.555:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                            else:
                                classified_class.append('Low')
                        else:
                            # node 0.215
                            if df['SCG'].iloc[i] > 0.215:
                                # node 0.795
                                if df['STR'].iloc[i] > 0.795:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                            else:
                                classified_class.append('very_low')
                #left part of node 13
                # bluegreen tables
                else:
                    # node 26 0.565
                    if df['PEG'].iloc[i] > 0.565:
                        classified_class.append('Middle')
                    else:
                        # node 52 0.5275
                        if df['STG'].iloc[i] > 0.5275:
                            # bluegreen 0.105
                            if df['SCG'].iloc[i] > 0.105:
                                # bluegreen 0.585
                                if df['STR'].iloc[i] > 0.585:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('very_low')
                            else:
                                # bluegreen 0.7
                                if df['STR'].iloc[i] > 0.7:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                        else:
                            # bluegreen 0.115
                            if df['SCG'].iloc[i] > 0.115:
                                classified_class.append('Middle')
                            else:
                                classified_class.append('Low')
            # left part node 6
            else:
                # node 12 0.405
                if df['LPR'].iloc[i] > 0.405:
                    # node 25 0.26
                    if df['PEG'].iloc[i] > 0.26:
                        # node 51 0.62
                        if df['STG'].iloc[i] > 0.62:
                            # brown 0.229
                            if df['SCG'].iloc[i] > 0.229:
                                # last brown 0.13
                                if df['STR'].iloc[i] > 0.13:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                            else:
                                # brown 0.2
                                if df['STR'].iloc[i] > 0.2:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                        else:
                            # brown 0.294
                            if df['SCG'].iloc[i] > 0.294:
                                classified_class.append('High')
                            else:
                                # brown 0.3
                                if df['STR'].iloc[i] > 0.2:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('High')
                    # left part brown table
                    else:
                        # node 50 0.385
                        if df['STG'].iloc[i] > 0.385:
                            # brown 0.185
                            if df['SCG'].iloc[i] > 0.185:
                                # last brown 0.29
                                if df['STR'].iloc[i] > 0.29:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('very_low')
                            else:
                                classified_class.append('very_low')
                        else:
                            # brown 0.195
                            if df['SCG'].iloc[i] > 0.195:
                                # brown 0.235
                                if df['STR'].iloc[i] > 0.235:
                                    classified_class.append('very_low')
                                else:
                                    classified_class.append('Low')
                            else:
                                classified_class.append('Low')
                # end of brown table
                # left part of node 12 
                # pink table
                else:
                    # node 24 0.505
                    if df['PEG'].iloc[i] > 0.505:
                        # node 49 0.445
                        if df['STG'].iloc[i] > 0.445:
                            classified_class.append('High')
                        else:
                            # node 49 0.445
                            if df['SCG'].iloc[i] > 0.2125:
                                classified_class.append('High')
                            else:
                                classified_class.append('Low')
                    # left part pink table
                    else:
                        # node 48 0.4
                        if df['STG'].iloc[i] > 0.4:
                            # pink node 0.1
                            if df['SCG'].iloc[i] > 0.1:
                                classified_class.append('Low')
                            else:
                                # node 0.335
                                if df['STR'].iloc[i] > 0.335:
                                    classified_class.append('very_low')
                                else:
                                    classified_class.append('Middle')
                        else:
                            # pink node 0.2565
                            if df['SCG'].iloc[i] > 0.2565:
                                # node 0.265
                                if df['STR'].iloc[i] > 0.265:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('very_low')
                            else:
                                classified_class.append('Low')
    ## End of right part OF THE THREE
    #
    #
    #
    #
    #
    # 
    
    else:
        # right node #2
        if df['SCG'].iloc[i] > 0.295:
            # right node #5
            if df['STR'].iloc[i] > 0.51: 
                # right node #11
                if df['LPR'].iloc[i] > 0.325:
                    # right node #23
                    if df['PEG'].iloc[i] > 0.28:
                        # node 47 0.219
                        if df['STG'].iloc[i] > 0.219:
                            # node 0.555
                            if df['SCG'].iloc[i] > 0.555:
                                classified_class.append('High')
                            else:
                                # node 0.84
                                if df['STR'].iloc[i] > 0.84:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('High')
                        else:
                            classified_class.append('High')
                    # left part of military green table
                    else:
                        # node 46 0.19
                        if df['STG'].iloc[i] > 0.19:
                            # node 0.63
                            if df['SCG'].iloc[i] > 0.63:
                                classified_class.append('Low')
                            else:
                                # node 0.84
                                if df['STR'].iloc[i] > 0.81:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                        else:
                            # node 0.455
                            if df['SCG'].iloc[i] > 0.455:
                                classified_class.append('Low')
                            else:
                                # node 0.68
                                if df['STR'].iloc[i] > 0.68:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                else:
                    # node 22, gray
                    if df['PEG'].iloc[i] > 0.64:
                        # node 45 gray
                        if df['STG'].iloc[i] > 0.29:
                            # right 0.51
                            if df['SCG'].iloc[i] > 0.51:
                                # 0.745
                                if df['STR'].iloc[i] > 0.745:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                            else:
                                # 0.725
                                if df['STR'].iloc[i] > 0.725:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('High')
                        else:
                            # left 0.33
                            if df['SCG'].iloc[i] > 0.33:
                                # 0.7
                                if df['STR'].iloc[i] > 0.7:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('High')
                            else:
                                #0.6
                                if df['STR'].iloc[i] > 0.6:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                    else:
                        # upper part of last left gray table
                        # node 44
                        if df['STG'].iloc[i] > 0.1275:
                            # 0.625
                            if df['SCG'].iloc[i] > 0.625:
                                # 0.79
                                if df['STR'].iloc[i] > 0.79:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                            else:
                                # 0.67
                                if df['STR'].iloc[i] > 0.67:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                        else:
                            # 0.475
                            if df['SCG'].iloc[i] > 0.475:
                                # 0.685
                                if df['STR'].iloc[i] > 0.685:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('very_low')
                            else:
                                classified_class.append('Middle')
            # left part umbral 5
            else:
                # right part umbral #10
                if df['LPR'].iloc[i] > 0.325:
                    # right part umbral #21
                    # blue white
                    if df['PEG'].iloc[i] > 0.255:
                        # node 43 0.21
                        if df['STG'].iloc[i] > 0.21:
                            # node blue white 0.665
                            if df['SCG'].iloc[i] > 0.665:
                                #node 0.235
                                if df['STR'].iloc[i] > 0.235:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                            else:
                                #node 0.245
                                if df['STR'].iloc[i] > 0.245:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                        else:
                            # node blue white 0.365
                            if df['SCG'].iloc[i] > 0.365:
                                #node 0.355
                                if df['STR'].iloc[i] > 0.355:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                            else:
                                classified_class.append('Low')
                    #   left part blue white table
                    else:
                        # node 42 0.2275
                        if df['STG'].iloc[i] > 0.2275:
                            # 0.625
                            if df['SCG'].iloc[i] > 0.625:
                                classified_class.append('Low')
                            else:
                                # 0.0.3
                                if df['STR'].iloc[i] > 0.3:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('very_low')
                        else:
                            # 0.47
                            if df['SCG'].iloc[i] > 0.47:
                                # 0.24
                                if df['STR'].iloc[i] > 0.24:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                            else:
                                classified_class.append('Low')   
                else:
                    # left part umbral 10 right Orange 0.585
                    if df['PEG'].iloc[i] > 0.585:
                        # node 41 0.22
                        if df['STG'].iloc[i] > 0.22:
                            # white 0.68
                            if df['SCG'].iloc[i] > 0.68:
                                # 0.38 orange table
                                if df['STR'].iloc[i] > 0.38:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('High')
                            else:
                                # 0.375 orange table
                                if df['STR'].iloc[i] > 0.375:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                        else:
                            # white 0.385
                            if df['SCG'].iloc[i] > 0.365:    
                                # 0.15 orange table
                                if df['STR'].iloc[i] > 0.15:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                            else:
                                # 0.205 orange table
                                if df['STR'].iloc[i] > 0.205:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('High')
                    # left Orange
                    else:
                        # node 40 0.18
                        if df['STG'].iloc[i] > 0.18:
                            # white 0.455
                            if df['SCG'].iloc[i] > 0.455:
                                # 0.395 orange  table
                                if df['STR'].iloc[i] > 0.395:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                            else:
                                # 0.34 orange table
                                if df['STR'].iloc[i] > 0.34:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('very_low')
                        else:
                            # Orange 0.365
                            if df['SCG'].iloc[i] > 0.365:
                                classified_class.append('Low')
                            else:
                                # 0.18 white low table
                                if df['STR'].iloc[i] > 0.18:
                                    classified_class.append('very_low')
                                else:
                                    classified_class.append('Low')
        ##left lef part
        # now, left part node 2 
        else:
            # node 4 0.405
            if df['STR'].iloc[i] > 0.405:
                # node 9 0.34
                if df['LPR'].iloc[i] > 0.34: 
                    # node 19 0.22
                    if df['PEG'].iloc[i] > 0.22:
                        # node 39 0.3075
                        if df['STG'].iloc[i] > 0.3075:  
                            # green 0.2625
                            if df['SCG'].iloc[i] > 0.2625:
                                # 0.62 green table
                                if df['STR'].iloc[i] > 0.62:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                            else:
                                # 0.65 green table
                                if df['STR'].iloc[i] > 0.65:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                        else:
                            # green node 0.1775
                            if df['SCG'].iloc[i] > 0.1775:
                                # 0.615 green table
                                if df['STR'].iloc[i] > 0.615:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                            else:
                                # Green node 0.465
                                if df['STR'].iloc[i] > 0.465:
                                    classified_class.append('Middle')
                                else:                               
                                    classified_class.append('Low')
                    # left part of node 19
                    else:
                        # node 38 0.26
                        if df['STG'].iloc[i] > 0.26:        
                            classified_class.append('Low')
                        else:
                            # node 0.12
                            if df['SCG'].iloc[i] > 0.12:
                                # node 0.6
                                if df['STR'].iloc[i] > 0.6:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('very_low')
                            else:
                                classified_class.append('Low')
                #left part of node 9
                # Blue tables
                else:
                    # node 18 0.575
                    if df['PEG'].iloc[i] > 0.575:
                        # node 37 0.165
                        if df['STG'].iloc[i] > 0.165:
                            # 0.19
                            if df['SCG'].iloc[i] > 0.19:    
                                classified_class.append('Middle')
                            else:
                                classified_class.append('High')
                        else:
                            # 0.0.1725
                            if df['SCG'].iloc[i] > 0.1725:
                                # 0.775
                                if df['SCG'].iloc[i] > 0.775:    
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                            else:
                                classified_class.append('High')
                    else:
                        # node 36 0.2515
                        if df['STG'].iloc[i] > 0.2515:
                            #  0.2275
                            if df['SCG'].iloc[i] > 0.2275:
                                #  0.725
                                if df['STR'].iloc[i] > 0.725:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                            else:
                                # bluegreen 0.62
                                if df['STR'].iloc[i] > 0.62:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('Middle')
                        else:
                            # bluegreen 0.1
                            if df['SCG'].iloc[i] > 0.1:
                                classified_class.append('very_low')
                            else:
                                # bluegreen 0.565
                                if df['STR'].iloc[i] > 0.565:
                                    classified_class.append('very_low')
                                else:
                                    classified_class.append('Middle')
            # left part node 4
            else:
                # node 9 0.365
                if df['LPR'].iloc[i] > 0.365:
                    # node 17 0.25
                    if df['PEG'].iloc[i] > 0.25:
                        # node 51 0.62
                        if df['STG'].iloc[i] > 0.62:
                            # brown 0.229
                            if df['SCG'].iloc[i] > 0.229:
                                # last brown 0.13
                                if df['STR'].iloc[i] > 0.13:
                                    classified_class.append('High')
                                else:
                                    classified_class.append('Middle')
                            else:
                                # brown 0.2
                                if df['STR'].iloc[i] > 0.2:
                                    classified_class.append('Middle')
                                else:
                                    classified_class.append('Low')
                        else:
                            # brown 0.294
                            if df['SCG'].iloc[i] > 0.294:
                                classified_class.append('High')
                            else:
                                # brown 0.3
                                if df['STR'].iloc[i] > 0.2:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('High')
                    # left part brown table
                    else:
                        # node 50 0.385
                        if df['STG'].iloc[i] > 0.385:
                            # brown 0.185
                            if df['SCG'].iloc[i] > 0.185:
                                # last brown 0.29
                                if df['STR'].iloc[i] > 0.29:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('very_low')
                            else:
                                classified_class.append('very_low')
                        else:
                            # brown 0.195
                            if df['SCG'].iloc[i] > 0.195:
                                # brown 0.235
                                if df['STR'].iloc[i] > 0.235:
                                    classified_class.append('very_low')
                                else:
                                    classified_class.append('Low')
                            else:
                                classified_class.append('Low')
                # end of brown table
                # left part of node 12 
                # pink table
                else:
                    # node 24 0.505
                    if df['PEG'].iloc[i] > 0.505:
                        # node 49 0.445
                        if df['STG'].iloc[i] > 0.445:
                            classified_class.append('High')
                        else:
                            # node 49 0.445
                            if df['SCG'].iloc[i] > 0.2125:
                                classified_class.append('High')
                            else:
                                classified_class.append('Low')
                    # left part pink table
                    else:
                        # node 48 0.4
                        if df['STG'].iloc[i] > 0.4:
                            # pink node 0.1
                            if df['SCG'].iloc[i] > 0.1:
                                classified_class.append('Low')
                            else:
                                # node 0.335
                                if df['STR'].iloc[i] > 0.335:
                                    classified_class.append('very_low')
                                else:
                                    classified_class.append('Middle')
                        else:
                            # pink node 0.2565
                            if df['SCG'].iloc[i] > 0.2565:
                                # node 0.265
                                if df['STR'].iloc[i] > 0.265:
                                    classified_class.append('Low')
                                else:
                                    classified_class.append('very_low')
                            else:
                                classified_class.append('Low')


accuracy =  metrics.accuracy_score(df['UNS'].tolist(),classified_class)
count = 0
for i in range(0,df.shape[0]):
  if df['UNS'].iloc[i] != df['TR'].iloc[i]:
    count+=1

print(f'Porcentaje de precisión: {accuracy*100}')
print(f'Se incurrió en {count} clases de {df.shape[0]}')

                 