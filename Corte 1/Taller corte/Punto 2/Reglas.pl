% Andres Felipe Wilches Torres - 20172020114
% Josue Alexander Nuñez Prada - 20172020071

:- dynamic si/1,no/1, grupo_trabajo/1, grupo_investigacion/1, verificar/1.
:- dynamic python/0, videojuegos/0, interfaz_Java/0, complexUD/0, verificar/0,
ciberseguridad/0, complejidad/0, mis/0, virtus/0, multi/0, arquisoft/0.
:- dynamic verificar/2.

iniciar:-
    write('Clasificacion de grupos de trabajo e investigacion basados en reglas'),nl,
    write('Escoga una de las siguientes opciones:'),nl,
    write('1) Escoga entre grupo de trabajo o grupo de investigacion'),nl,
    write('2) Adicionar grupo de trabajo / investigacion'),nl,
    write('3) Eliminar grupo de trabajo / investigacion'),nl,nl,
    read(OPCION),
    menu(OPCION).

% opcion para buscar gp trabajo / investigacion
menu_grupo(X) :-
    X == 1 ->
        write('Grupo trabajo'),nl,
        write('Indica tus gustos'),nl,
        grupo_trabajo(GRUPO),
        write('Tu grupo de trabajo ideal es: ¡'),
        write(GRUPO),
        write('!');
    X == 2 ->
        write('Grupo investigacion'),nl,
        write('Indica tus gustos'),nl,
        grupo_investigacion(GRUPO),
        write('Tu grupo de investigacion ideal es: ¡'),
        write(GRUPO),
        write('!').

% opcion para borrar gp trabajo / investigacion
menu_eliminar(X) :-
    X == 1 ->
        eliminar_trabajo;
    X == 2 ->
        eliminar_investigacion.

% opcion para guardar gp trabajo / investigacion
menu_guardar(X) :-
    X == 1 ->
        crear_trabajo;
    X == 2 ->
        crear_investigacion.

% opciones menu principal
menu(X) :-
    X == 1 ->
        write('Escoga el tipo de grupo:'),nl,
        write('1) Grupo de trabajo'),nl,
        write('2) Grupo de investigacion'),nl,nl,
        read(OPCION),
        menu_grupo(OPCION);
    X == 2 ->
        write('Escoga el tipo de grupo a crear:'),nl,
        write('1) Grupo de trabajo'),nl,
        write('2) Grupo de investigacion'),nl,nl,
        read(OPCION),
        menu_guardar(OPCION);
    X == 3 ->
        write('Escoga el tipo de grupo a borrar:'),nl,
        write('1) Grupo de trabajo'),nl,
        write('2) Grupo de investigacion'),nl,nl,
        read(OPCION),
        menu_eliminar(OPCION).

% Reglas grupo de investigacion
grupo_investigacion(mis) :- mis, !.
grupo_investigacion(virtus) :- virtus, !.
grupo_investigacion(complexUD) :- complexUD, !.
grupo_investigacion(multi) :- multi, !.
grupo_investigacion(arquisoft) :- arquisoft.

% Reglas grupo de trabajo
grupo_trabajo(videojuegos) :- videojuegos, !.
grupo_trabajo(python) :- python, !.
grupo_trabajo(interfaz_Java) :- interfaz_Java, !.
grupo_trabajo(ciberseguridad) :- ciberseguridad, !.
grupo_trabajo(complejidad) :- complejidad.

%----------------------------------------------------------
% Preguntas para saber si te gusta programar
programador :- verificar('¿Eres una persona con alta dosis de autonomia?').
programador2 :- verificar('¿Eres una persona creativa?').
programador3 :- verificar('¿Eres una persona creativa?').
programador4 :- verificar('¿Eres una persona comprometida?').
programador5 :- verificar('¿Eres una persona ambiciosa?').

%------------------- GRUPOS TRABAJO ---------------------
% Para videojuegos
videojuegos    :- programador,
    			  programador2,
				  programador3,
    			  programador4,
    			  programador5,
    			  verificar('¿Te gusta crear entretenimiento para las personas?'),
    			  verificar('¿Te llama la atencion los videojuegos?').
%-------------------------------------------------------------
% Para python
python		   :- programador,
    			  programador2,
				  programador3,
    			  programador4,
    			  programador5,
    			  verificar('¿Te gustan los lenguajes de programacion con tipado dinamico ?'),
    			  verificar('¿Te gusta python?').
%-------------------------------------------------------------
% Para interfaz_Java
interfaz_Java		   :- programador,
    			  programador2,
				  programador3,
    			  programador4,
    			  programador5,
    			  verificar('¿Te gustan los lenguajes de programacion con tipado fuerte?'),
    			  verificar('¿Te gustaria aprender a hacer interfaces?').
%-------------------------------------------------------------
% Para ciberseguridad
ciberseguridad         :- programador,
    			  programador2,
				  programador3,
    			  programador4,
    			  programador5,
    			  verificar('¿Te gustaria aprender de vulnerabilidades informaticas?').
%-------------------------------------------------------------
% Para complejidad
complejidad		   :- verificar('¿Te gustan las matematicas?'),
    				  verificar('¿Te gustaria conocer del funcionamiento del mundo?'),
    				  verificar('¿Te gustaria aplicar inteligencia artificial a tus programas?'),
    				  verificar('¿Te llama la atencion la entropia?').


%------------------- GRUPOS INVESTIGACION ---------------------
% Para complexUD
complexUD    :- verificar('¿Te gustan las matematicas?'),
    				  verificar('¿Te gustaria conocer del funcionamiento del mundo?'),
    				  verificar('¿Te gustaria aplicar inteligencia artificial a tus programas?'),
    				  verificar('¿Te llama la atencion la entropia?'),
                      verificar('¿Te gustaria crear articulos basado en esa linea?').
%-------------------------------------------------------------
%R9 para Arquisoft
arquisoft		   :- programador,
    			  programador2,
				  programador3,
    			  programador4,
    			  programador5,
    			  verificar('¿Te gustaria crear software desde el diseño de forma elegante?').
%-------------------------------------------------------------
% Para MULTI
multi		   :- programador,
    			  programador2,
				  programador3,
                  verificar('¿Te gustan los sistemas embebidos?'),
                  verificar('¿Te gusta la revision sistematica de la literatura abordada en campos como la robotica?').
%-------------------------------------------------------------
% Para VIRTUS
virtus          :- programador,
    			  programador2,
				  programador3,
    			  programador4,
    			  programador5,
    		      verificar("¿Te gustan los ambientes virtuales o OVA's?").
%-------------------------------------------------------------
% Para MIS
mis		   :- verificar('¿Te gustan las matematicas?'),
    				  verificar('¿Te gustaria investigar del funcionamiento de sistemas?'),
    				  verificar('¿Te gustaria aplicar inteligencia artificial?').

/* pregunta repetitiva */

preguntar(Question) :-
    write(Question),
    write('? '),
    read(Response),
    nl,
    ( (Response == si ; Response == s ;Response == 1)
      ->
       assert(si(Question)) ;
       assert(no(Question)), fail).

/* verificación*/
verificar(S) :- (si(S) -> true ;
               (no(S)  -> fail ;
               preguntar(S))).

%--------------------------ELIMINACION DE GRUPOS-----------------------------------

eliminar_trabajo:-
    write('Digite el grupo de trabajo: '),
    read(Trabajo),
    retractall(grupo_trabajo(Trabajo)), retractall(Trabajo),
    write('Grupo eliminado'),
    grupo_trabajo(Trabajo).

eliminar_investigacion:-
    write('Digite el grupo de investigacion: '),
    read(Investigacion),
    retractall(grupo_investigacion(Investigacion)), retractall(Investigacion),
    write('Grupo eliminado'),
    grupo_investigacion(Investigacion).

%--------------------------CREACION DE GRUPOS-----------------------------------

crear_trabajo:-
    write('Digite el nombre del grupo de trabajo: '),
    read(Trabajo),
    write('Digite cuantas reglas piensa agregar: '),
    read(Cantidad),
    crear(Cantidad,X),
    Y = (grupo_trabajo(Trabajo) :- X),
    assert(Y),
    write('Grupo creado').

crear_investigacion:-
    write('Digite el nombre del grupo de investigacion: '),
    read(Investigacion),
    write('Digite cuantas reglas piensa agregar: '),
    read(Cantidad),
    crear(Cantidad,X),
    Y = (grupo_investigacion(Investigacion) :- X),
    assert(Y),
    write('Grupo creado').

% Para terminar recursividad

crear(0, F, Regla, Q, Grupo):-
    Y = (Q :- F,Regla),
    assertz(Y),
    Grupo = Q.

% Caso inicial

crear(C, Grupo):-
    write(C),write(").Pregunta: "),read(Question),
    Ctemp is C-1,
    F = verificar(Question),
    crear(Ctemp,F, Question, Grupo).

% Preguntas recursividad para el resto de casos

crear(C, Regla, 0, Grupo):-
    write(C),write(").Pregunta: "),read(Question),
    Ctemp is C-1,
    F = verificar(Question),
    crear(Ctemp, F, Regla, Question, Grupo).

% Guardar caso inicial

crear(C, F, Q, Grupo):-
    Y = (Q :- F),
    assertz(Y),
    crear(C, Q, 0, Grupo).

% Guardar recursivo

crear(C, F, Regla, Q, Grupo):-
    Y = (Q :- F,Regla),
    assertz(Y),
    crear(C, Q, 0, Grupo).

% Para borrar variables al reiniciar

undo :- grupo_trabajo(_),fail.
undo :- grupo_investigacion(_),fail.
undo :- si(_),no(_),fail.
undo.