:- nl,nl,write('Digite \'preguntar.\' para iniciar:  ?- preguntar. '), nl,nl,nl.

/* tiene_pelo:(_). */

% R1
preguntar:- write('El animal tiene pelo? (si/no) '), 
            read(Respuesta),nl, (
                (Respuesta==si)->preguntarSiComeCarne;
                                                    preguntarSiDaLeche).

% R2
preguntarSiDaLeche:- write('El animal da leche? (si/no) '), 
                      read(Respuesta),nl, ((Respuesta==si)->preguntarSiComeCarne;fail).

% R5
preguntarSiComeCarne:- write('El animal es mamifero'), nl, write('El animal come carne? (si/no) '), 
                        read(Respuesta),nl, (
                                        (Respuesta==si)-> preguntarSiTieneColorLeonado;preguntarSiTieneDientesAgudos).

% R6
preguntarSiTieneDientesAgudos:- write('El animal tiene dientes agudos? (si/no) '), 
                            read(Respuesta),nl, (
                                              (Respuesta==si)->preguntarSiTieneGarras;preguntarSiTienePezunas).        
% R6
preguntarSiTieneGarras:- write('El animal tiene garras? (si/no) '), 
                            read(Respuesta),nl, (
                                      (Respuesta==si)->preguntarSiTieneOjosMiraDelante;fail).
% R6
preguntarSiTieneOjosMiraDelante:- write('El animal tiene ojos que miran hacia delante? (si/no) '), 
                            read(Respuesta),nl, (
                                      (Respuesta==si)->preguntarSiTieneColorLeonado;fail).                             

% R7
preguntarSiTienePezunas:- write('El animal tiene pezuñas? (si/no) '), 
                            read(Respuesta),nl, (
                              (Respuesta==si)->preguntarSiTienePatasLargas;preguntarSiRumia).

% R8
preguntarSiRumia:- write('El animal rumia? (si/no) '), 
                            read(Respuesta),nl, ((Respuesta==si)->preguntarSiTienePatasLargas;fail). 

% R9
preguntarSiTieneColorLeonado:- write('El animal tiene color leonado? (si/no) '), 
                            read(Respuesta),nl, (
                                (Respuesta==si)->preguntarSiTieneManchasOscuras;fail).     

% R9
preguntarSiTieneManchasOscuras:- write('El animal tiene manchas oscuras? (si/no) '), 
                            read(Respuesta),nl, (
                              (Respuesta==si)->write('El animal es una Onza');preguntarSiTieneFranjasNegras).   

% R10
preguntarSiTieneFranjasNegras:- write('El animal tiene franjas negras? (si/no) '), 
                            read(Respuesta),nl, ((Respuesta==si)->write('El animal es un Tigre');
                                                                  write('Animal no encontrado')). 

% R11
preguntarSiTienePatasLargas:- write('El animal tiene patas largas? (si/no) '), 
                            read(Respuesta),nl, (
                              (Respuesta==si)->preguntarSiTieneCuelloLargo;preguntarSiTieneColorBlanco).

% R11
preguntarSiTieneCuelloLargo:- write('El animal tiene cuello largo? (si/no) '), 
                            read(Respuesta),nl, ((Respuesta==si)->preguntarSiTieneColorLeonado2;fail).                                                                                                                                                                

% R11
preguntarSiTieneColorLeonado2:- write('El animal tiene color leonado? (si/no) '), 
                            read(Respuesta),nl, ((Respuesta==si)->preguntarSiTieneManchasOscuras2;fail).   

% R11
preguntarSiTieneManchasOscuras2:- write('El animal tiene manchas oscuras? (si/no) '), 
                            read(Respuesta),nl, (
                              (Respuesta==si)->write('El animal es una Jirafa');write('Animal no encontrado')).

% R12
preguntarSiTieneColorBlanco:- write('El animal tiene color blanco? (si/no) '), 
                            read(Respuesta),nl, (
                              (Respuesta==si)->preguntarSiTieneFranjasNegras2;write('Animal no encontrado')).                           

% R12
preguntarSiTieneFranjasNegras2:- write('El animal tiene franjas negras? (si/no) '), 
                            read(Respuesta),nl, ((Respuesta==si)->write('El animal es una Cebra');
                                                                  write('Animal no encontrado')).                               