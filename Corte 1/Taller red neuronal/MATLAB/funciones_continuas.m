% Aplicacion red neuronal funciones continuas
close all
clear all
warning off

%% Creacion red neuronal con funciones continuas (Andres)
% se definen entradas y salidas
input=[1 1 1 0 1 0 1 1; 1 1 1 0 0 1 0 1; 1 1 1 0 1 1 1 0; 1 1 1 1 1 0 0 0; 1 1 1 0 1 1 1 1; 1 1 1 1 1 0 0 1];
target=[0 1 0 0 0 0 0 1; 0 1 0 0 1 1 1 0; 0 1 0 0 0 1 0 0; 0 1 0 1 0 0 1 0; 0 1 0 0 0 1 0 1; 0 1 0 1 0 0 1 1];
% tipo de capas
funcact={'logsig','logsig','logsig','logsig','logsig','logsig','logsig','logsig','logsig','logsig'};
% red neuronal FF, recibe entrada, salida, cantidad de neuronas capas
% ocultas y el tipo de capas
net=newff(input, target, [30 30 30 30 30 30 30 30 30 30], funcact); 
% entrenamiento
net = train(net, input, target);
% simulacion
output=sim(net,input);
% resultado
display(output);

close all
clear all

%% Creacion red neuronal con funciones continuas(Josue)
% se definen entradas y salidas
input=[1 1 1 0 0 0 0 0; 1 1 1 0 0 1 0 1; 1 1 1 1 1 0 0 1; 1 1 1 1 1 1 1 1; 1 1 1 0 1 1 1 1];
target=[0 1 0 0 1 0 1 0; 0 1 0 0 1 1 1 1; 0 1 0 1 0 0 1 1; 0 1 0 1 0 1 0 1; 0 1 0 0 0 1 0 1];
% tipo de capas}
funcact={'logsig','logsig','logsig','logsig','logsig','logsig','logsig','logsig','logsig','logsig'};
% red neuronal FF, recibe entrada, salida, cantidad de neuronas capas
% ocultas y el tipo de capas
net=newff(input, target, [30 30 30 30 30 30 30 30 30 30], funcact); 
% entrenamiento
% entrenamiento
net = train(net, input, target);
% simulacion
output=sim(net,input);
% resultado
display(output);