% Aplicacion red neuronal perceptron
close all
clear all
warning off

%% Creacion red neuronal perceptron para Andres
X=[1 1 1 0 1 0 1 1; 1 1 1 0 0 1 0 1; 1 1 1 0 1 1 1 0; 1 1 1 1 1 0 0 0; 1 1 1 0 1 1 1 1; 1 1 1 1 1 0 0 1];
D=[0 1 0 0 0 0 0 1; 0 1 0 0 1 1 1 0; 0 1 0 0 0 1 0 0; 0 1 0 1 0 0 1 0; 0 1 0 0 0 1 0 1; 0 1 0 1 0 0 1 1];
plotpv(X,D);
red=newp([0 1; 0 1],1);
red.iw {1,1}=[1,1];
red.b{1}=0.5;
pesos=red.iw{1,1};
bias = red.b{1};
plotpc(pesos,bias);
red=train(red,X,D);
figure;
pesos=red.iw{1,1};
bias = red.b{1};
plotpv(X,D);
plotpc(pesos,bias); 
prueba=[0;0];
a=sim(red,prueba);
prueba=[1;1];
a=sim(red,prueba);